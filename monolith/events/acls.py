from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    d = {}

    url = f"https://api.pexels.com/v1/search?query={city},{state}&per_page=1"
    headers = {'authorization': PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    content = json.loads(r.content)
    try:
        d["picture_url"] = content["photos"][0]["url"]
    except:
        return None

    return d


def get_weather_data(city, state):
    d = {}

    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    headers = {'authorization': "35a213962ff94c41aef5f9eb108b1162"}

    r1 = requests.get(geocoding_url, headers=headers)
    r1_content = json.loads(r1.content)
    try:
        lat = r1_content[0]["lat"]
        lon = r1_content[0]["lon"]
    except:
        return None

    current_weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    r2 = requests.get(current_weather_url, headers=headers)
    r2_content = json.loads(r2.content)

    try:
        d["temp"] = round(((r2_content["main"]["temp"] - 273.15) * 9 / 5) + 32, 2)
        d["description"] = r2_content["weather"][0]["description"]
    except:
        return None

    return d


# def get_state_name(abbr):
#     states = {
#         "AL": "Alabama",
#         "AK": "Alaska",
#         "AZ": "Arizona",
#         "AR": "Arkansas",
#         "CA": "California",
#         "CO": "Colorado",
#         "CT": "Connecticut",
#         "DE": "Delaware",
#         "FL": "Florida",
#         "GA": "Georgia",
#         "HI": "Hawaii",
#         "ID": "Idaho",
#         "IL": "Illinois",
#         "IN": "Indiana",
#         "IA": "Iowa",
#         "KS": "Kansas",
#         "KY": "Kentucky",
#         "LA": "Louisiana",
#         "ME": "Maine",
#         "MD": "Maryland",
#         "MA": "Massachusetts",
#         "MI": "Michigan",
#         "MN": "Minnesota",
#         "MS": "Mississippi",
#         "MO": "Missouri",
#         "MT": "Montana",
#         "NE": "Nebraska",
#         "NV": "Nevada",
#         "NH": "New Hampshire",
#         "NJ": "New Jersey",
#         "NM": "New Mexico",
#         "NY": "New York",
#         "NC": "North Carolina",
#         "ND": "North Dakota",
#         "OH": "Ohio",
#         "OK": "Oklahoma",
#         "OR": "Oregon",
#         "PA": "Pennsylvania",
#         "RI": "Rhode Island",
#         "SC": "South Carolina",
#         "SD": "South Dakota",
#         "TN": "Tennessee",
#         "TX": "Texas",
#         "UT": "Utah",
#         "VT": "Vermont",
#         "VA": "Virginia",
#         "WA": "Washington",
#         "WV": "West Virginia",
#         "WI": "Wisconsin",
#         "WY": "Wyoming",
#     }

#     return states.get(abbr)
